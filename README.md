# README HOW TO GIT 

## creacio de un repositori desde consola

* pasos a seguir 

crea un directori que sera la arrel del repositori

[isx47262285@i15 Documents]$ mkdir prova_git
[isx47262285@i15 Documents]$ cd prova_git/
[isx47262285@i15 prova_git]$ ll
total 0

* inicializar el repositori amb la seguent ordre

[isx47262285@i15 prova_git]$ git init
Initialized empty Git repository in /home/users/inf/hisx2/isx47262285/Documents/prova_git/.git/
[isx47262285@i15 prova_git]$ ll
total 4
-rw-r--r--. 1 isx47262285 hisx2 53 Apr 24 08:46 README.md

* configuracio del usuari 

[isx47262285@i15 prova_git]$ git config --global user.email "rober_72004@gmail.com"
[isx47262285@i15 prova_git]$ git config --global user.name "roberto altamirano" 
[isx47262285@i15 prova_git]$ git config --global push.default simple 

* configuracio del repositori en remot 

previament se ha de crea el repositori via web en la pagina de gitlab

despress pasem a configurar de manera local el repositori remot 

[isx47262285@i15 prova_git]$ git remote add prova  git@gitlab.com:isx47262285/prova_git.git
[isx47262285@i15 prova_git]$ git remote -v
prova	git@gitlab.com:isx47262285/prova_git.git (fetch)
prova	git@gitlab.com:isx47262285/prova_git.git (push)


de aquesta manera el nostre repositori local sap a on ha de anar en remot


> cal dir que tenim configurat el remote amb una direccio de repositori que utiliza claus ssh, per tant si tot va be, esperem que peti i pasarem al pass de configuracio de les clau ssh.

* ara afegim tot lo que tenim amb les ordres seguents

[isx47262285@i15 prova_git]$ git add  .
[isx47262285@i15 prova_git]$ git commit -am "hola" 

amb aixo aconseguim que tot lo que fem en local el tingui preparat per pujar al repositori remot 

```
[isx47262285@i15 prova_git]$ git push
fatal: No configured push destination.
Either specify the URL from the command-line or configure a remote repository using

    git remote add <name> <url>

and then push using the remote name

    git push <name>
```
aquesta primera part ens diu que no te be configurat el repositori y que no sap quin es el repositori push 

provem amb el repositori que en afegit "prova" , si no sabs com ho has denominat pots fer servir la ordre "git remote -v" 


```
[isx47262285@i15 prova_git]$ git push prova 
fatal: The current branch master has no upstream branch.
To push the current branch and set the remote as upstream, use

    git push --set-upstream prova master

[isx47262285@i15 prova_git]$  git push --set-upstream prova master
git@gitlab.com: Permission denied (publickey).
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
```

bueno tot sembla que ens ha dit que hem de utilitzar una ordre per fer la configuracion del push, " git push --set-upstream prova master" 

i per acaba, sembla que funciona pero com que les claus no hi son les hem de configurar.


* GENERA LES CLAUS SSH 
```
[isx47262285@i15 prova_git]$  ssh-keygen -o -t rsa -b 4096 -C "prova_clau"
Generating public/private rsa key pair.
Enter file in which to save the key (/home/users/inf/hisx2/isx47262285/.ssh/id_rsa): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/users/inf/hisx2/isx47262285/.ssh/id_rsa.
Your public key has been saved in /home/users/inf/hisx2/isx47262285/.ssh/id_rsa.pub.
The key fingerprint is:
SHA256:8jirpphoQa3N/zvAcUA07mmbGKUYkl7kz1kpT3K3IXg prova_clau
The key's randomart image is:
+---[RSA 4096]----+
|   .o+           |
| .o ..o .        |
|o..o *.E o       |
|o.+.*.@.o o      |
|.o+o.B+.S.       |
| o o+oo+         |
|  ...o+ .        |
|.+  .. +         |
|= .o..ooo        |
+----[SHA256]-----+


```
hem de agafar la part de la clau publica local y afegir-la en la pagina de gitlab  en el la part de preferences, ssh keys

exemple :
```
[isx47262285@i15 prova_git]$ cat ~/.ssh/id_rsa.pub 
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDRfTXR/8/uLpkR/EkpdqFhQddhsbt82qUZZOwd70qcyOKeRuJeebjvKPkdbUYwEyLh1yLTy6EVJ6hP71nuytLyUl/BcNMl2jKL4mjcXS+PlJS69Y0luUkbi4RQRzhp32qOmduer6Bcl8mlLQYmHhMR7tGoIXKA5+xZc7DrRJlB2srFgFsrgZCN0ccNhN9z4BDzkkYLUk2yM+ICrVMYlh9LdCqMpggpz8zCO7QasVFNG3F1fe+/iAo9H++tGIyXQLvXmpkrqEhmeZoXTY1ClhAgYoGTNtG1kJWWLagTTcQqC89v7y7Jz7grDI+0pZlUbvrgrPXI2yiHbnZ19vL0y5omWaiPLgZze9sIDFEb3iN2aB+gK3Zr4ax616eMAHlDUWcKNkrLgNUN5HK3X3w/f4NLOL/kL/szPTynze10Iusz0GBQoSY1YyGTKPwPGE/kNuLvnKWOoapQiPxufgxnk2fX6dr6Ga3zDfpjP8FJUe5IQZIXu2YaPrWVU+m8lACIMdb4XbM/aT83grqh/4t61/SBBaiy9t5TgXHl9d51g5skBfhBRSqzZLpJfEv0i4AOsh15Lz1DWCf5Mp28PolgmUH7KS81eevdYgP5vMHGurg/TaR6AL9i4BMODu9lhUfUoyeW47mvcj5UEAlXb3ZSG2yqXqCM+AfMhYrdJiyD7BfAcw== prova_clau
```


tornem a provar de fer el push.
```
[isx47262285@i15 prova_git]$  git push --set-upstream prova master
sign_and_send_pubkey: signing failed: agent refused operation
git@gitlab.com: Permission denied (publickey).
fatal: Could not read from remote repository.

Please make sure you have the correct access rights
and the repository exists.
```

continuem tenin un problema, pero ara tenim claus. 

per solucionar el problema de les claus que continuan malament fem el seguent pass..... 
```
[isx47262285@i15 prova_git]$ ssh -T git@gitlab.com
sign_and_send_pubkey: signing failed: agent refused operation
git@gitlab.com: Permission denied (publickey).
```

ens diu que no te access a la clau.... per tan per resoldre se ha de afegir la clau al git de manera local i desde consola.

```

[isx47262285@i15 prova_git]$ eval "$(ssh-agent -s)" 
Agent pid 7078
[isx47262285@i15 prova_git]$ ssh-add 
Identity added: /home/users/inf/hisx2/isx47262285/.ssh/id_rsa (prova_clau)
[isx47262285@i15 prova_git]$ ssh -T git@gitlab.com
Welcome to GitLab, @isx47262285!

```
ara si que tenim access amb claus ssh !!  

```
[isx47262285@i15 prova_git]$  git push --set-upstream prova master
To gitlab.com:isx47262285/prova_git.git
 ! [rejected]        master -> master (non-fast-forward)
error: failed to push some refs to 'git@gitlab.com:isx47262285/prova_git.git'
hint: Updates were rejected because the tip of your current branch is behind
hint: its remote counterpart. Integrate the remote changes (e.g.
hint: 'git pull ...') before pushing again.
hint: See the 'Note about fast-forwards' in 'git push --help' for details.
```

hay conflicto con lo cual hay que hacer un pull 

[isx47262285@i15 prova_git]$ git pull prova master 
From gitlab.com:isx47262285/prova_git
 * branch            master     -> FETCH_HEAD
fatal: refusing to merge unrelated histories

per solucionar:


[isx47262285@i15 prova_git]$ git pull --allow-unrelated-histories  prova master 
From gitlab.com:isx47262285/prova_git
 * branch            master     -> FETCH_HEAD
Merge made by the 'recursive' strategy.
 README.md | 6 ++++++
 1 file changed, 6 insertions(+)
 create mode 100644 README.md


[isx47262285@i15 prova_git]$ git pull --allow-unrelated-histories  prova master 
From gitlab.com:isx47262285/prova_git
 * branch            master     -> FETCH_HEAD
Merge made by the 'recursive' strategy.
 README.md | 6 ++++++
 1 file changed, 6 insertions(+)
 create mode 100644 README.md

[isx47262285@i15 prova_git]$ git add . 
[isx47262285@i15 prova_git]$ git commit -am "funciona" 
On branch master
nothing to commit, working tree clean



* ha funcionat!! 

[isx47262285@i15 prova_git]$ git push prova master 
Counting objects: 10, done.
Delta compression using up to 8 threads.
Compressing objects: 100% (8/8), done.
Writing objects: 100% (10/10), 3.39 KiB | 3.39 MiB/s, done.
Total 10 (delta 1), reused 0 (delta 0)
To gitlab.com:isx47262285/prova_git.git
   e9e037c..b215d43  master -> master


> planteamos un pequeño problema y esque nos fuerza a decir todo el rato desde que branca git a que branca volem pujar, podem mirar de millorar 

### CREACIO DE UN REPOSITORI DESDE PAGINA WEB.


* primero 

acedemos a nuestro navegador de internet y entramos en la pagina de gitlab.com nos logeamos y procedemos a crear un repositorio nuevo 

en la parte superior derecha tenemos un boton 'New project' clicamos y procedemos a crear el repositorio, escojemos un nombre para el repositorio,
le damos a la opcion de repositorio publico ( existe el privado ) esto segun nuestras preferencias, si queremos que sea un repositorio a comprartir y lo vean
mas personas (publico) o queremos que sea un repositorio personal al que solo podamos acceder nosotros  para nuestro desarrollo (privado) .

tambien podemos añadir una descripcion del propio repositorio. 

una vez creado....  

tenemos un repositorio en la nube pero no en la maquina local, como ya tenemos las claves ssh creadas antes haremos los siguientes pasos 

1. entramos en el repositorio
2. abrimos la pestaña de 'clone' (parte superior derecha del repositorio)
3. seleccionamos y copiamos el enlace de 'clone con ssh' 
4. abrimos la consola y ejecutamos '$ git clone url' (url que hemos copiado)

desde consola:

```
$ git clone git@gitlab.com:isx47262285/m09-uf2.git
```

ejemplo:

```
[isx47262285@i15 robertoAltamirano]$ git clone git@gitlab.com:isx47262285/m09-uf2.git
Cloning into 'm09-uf2'...
remote: Enumerating objects: 25, done.
remote: Counting objects: 100% (25/25), done.
remote: Compressing objects: 100% (24/24), done.
remote: Total 25 (delta 7), reused 0 (delta 0)
Receiving objects: 100% (25/25), 128.38 KiB | 515.00 KiB/s, done.
Resolving deltas: 100% (7/7), done.
```


entramos en nuestro repositorio

```
$ cd m09-uf2
```

por ultimo hacemos los cambios que hagan falta dentro del repositorio y haremos las ordenes de actualizacion para subir a la nube gitlab

```
$ git add .     > para añadir el nuevo contenido

$ git commit -am "comentario"    > para guardar los cambios hecho

$ git push  > para subir la informacion al gitlab
```
```
[isx47262285@i15 robertoAltamirano]$ cd m09-uf2/
[isx47262285@i15 m09-uf2]$ ls > prova.txt
[isx47262285@i15 m09-uf2]$ git add .
[isx47262285@i15 m09-uf2]$ git commit -am "es una prueba" 
[master 4dfda58] es una prueba
 1 file changed, 4 insertions(+)
 create mode 100644 prova.txt
[isx47262285@i15 m09-uf2]$ git push
```

todo perfecto !

